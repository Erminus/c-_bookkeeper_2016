using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;

namespace Labb2
{
	[Activity (Label = "Labb2.2.2016", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		// Private button
		private Button newEventButton;
		private Button showAllEvents;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			Xamarin.Insights.Initialize (XamarinInsights.ApiKey, this);
			base.OnCreate (savedInstanceState);
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			// Get our button from the layout resource,
			newEventButton = FindViewById<Button> (Resource.Id.new_event_button);
			showAllEvents = FindViewById<Button> (Resource.Id.all_events_button);

			// Handle add new events button
			newEventButton.Click += delegate {
				
				Intent intent = new Intent (this, typeof(NewEventActivity));
				StartActivity (intent);
			};
			// Handle show all events button
			showAllEvents.Click += delegate {
				Intent intent = new Intent (this, typeof(EventListActivity));
				StartActivity (intent);
			};
		
		}
	}
}
