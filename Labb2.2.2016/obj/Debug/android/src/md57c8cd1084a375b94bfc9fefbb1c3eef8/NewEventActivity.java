package md57c8cd1084a375b94bfc9fefbb1c3eef8;


public class NewEventActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Labb2.NewEventActivity, Labb2.2.2016, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", NewEventActivity.class, __md_methods);
	}


	public NewEventActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == NewEventActivity.class)
			mono.android.TypeManager.Activate ("Labb2.NewEventActivity, Labb2.2.2016, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
