﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Org.Apache.Http.Message;
using Android.Util;

namespace Labb2
{
	[Activity (Label = "NewEvent")]			
	public class NewEventActivity : Activity
	{
		//Radiobuttons
		private RadioButton incomeRadio, spendRadio;
		// All edittext
		private EditText editDate, describeText, addAmount;
		// All Spinners
		private Spinner accountTypeSpinner, toFromAcountSpinner, taxSpinner;
		// Buttons
		private Button addNewEvent;
		// Class Instance
		private Entry inputEntry;
		// Privat bookkeepermanager
		private BookKeeperManager bookKeeper = BookKeeperManager.Instance;
		// Array Addapters for accountType, Money account, and tax persentage
		private ArrayAdapter typeAdapter, moneyAdapter, taxAdapter;

		private int chosenTax;
		private int chosenType, chosenAccount;


		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.NewEvent);
			// Radiobuttons 1.2 och 1.3>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			incomeRadio = FindViewById<RadioButton> (Resource.Id.income_radio);
			spendRadio = FindViewById<RadioButton> (Resource.Id.spending_radio);

			// EditText 1 = date input, 2 = describe entry, 3 = input amount>>>>>>>>>>>>>>>>>
			editDate = FindViewById<EditText> (Resource.Id.date_input_text);
			describeText = FindViewById<EditText> (Resource.Id.explain_input_text);
			addAmount = FindViewById<EditText> (Resource.Id.amount_input_text);

			// Spinner 1 = account type, 2 = toFrom spinner, 3 = Tax spinner>>>>>>>>>>>>>>>>>
			accountTypeSpinner = FindViewById<Spinner> (Resource.Id.account_type_spinner);
			toFromAcountSpinner = FindViewById<Spinner> (Resource.Id.to_from_acount_spinner);
			taxSpinner = FindViewById<Spinner> (Resource.Id.tax_spinner);

			// Button add new entry
			addNewEvent = FindViewById<Button> (Resource.Id.add_new_event_button);

			// Insert spinners >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			UpdateAccountName (true);

			incomeRadio.Click += delegate {
				UpdateAccountName (true);
			};
			spendRadio.Click += delegate {
				UpdateAccountName (false);
			};

			moneyAdapter = new ArrayAdapter (this, Android.Resource.Layout.SimpleSpinnerItem, bookKeeper.MoneyAccounts);
			moneyAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			toFromAcountSpinner.Adapter = moneyAdapter;

			taxAdapter = new ArrayAdapter (this, Android.Resource.Layout.SimpleSpinnerItem, bookKeeper.TaxRates);
			taxAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			taxSpinner.Adapter = taxAdapter;

			// Handle add event button>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			addNewEvent.Click += delegate {

				inputEntry = new Entry ();
				inputEntry.Date = editDate.Text;
				inputEntry.Account = chosenAccount; 
				inputEntry.Type = chosenType;
				inputEntry.Description = describeText.Text;
				inputEntry.TaxRate = chosenTax;
				inputEntry.TotalAmount = Double.Parse (addAmount.Text);
										
				bookKeeper.AddEntry (inputEntry);
			};
		}

		// Accounts

		private void account_Spinner (object sender, AdapterView.ItemSelectedEventArgs e)
		{

			if (incomeRadio.Checked) {
				chosenType = bookKeeper.IncomeAccounts [e.Position].Number;
			} else {
				chosenType = bookKeeper.SpendAccounts [e.Position].Number;
			}

		}

		// To  and from account
		private void toFrom_Spinner (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			chosenAccount = bookKeeper.MoneyAccounts [e.Position].Number;
		}
		// Tax positions
		private void tax_Spinner (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			chosenTax = bookKeeper.TaxRates [e.Position].TaxPersentage;
		}

		private void UpdateAccountName (bool income)
		{
			typeAdapter = new ArrayAdapter (this, Android.Resource.Layout.SimpleSpinnerItem, 
				income ? bookKeeper.IncomeAccounts : bookKeeper.SpendAccounts);
			typeAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			accountTypeSpinner.Adapter = typeAdapter;

			// Spinner toasts
			accountTypeSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (account_Spinner);
			toFromAcountSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (toFrom_Spinner);
			taxSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (tax_Spinner);
		}
			
	}
}

