﻿using System;
using Android.Widget;
using Android.Views;
using Android.App;
using System.Collections.Generic;

namespace Labb2
{
	public class EventAdapter : BaseAdapter
	{

		// Declare private varible
		// Activity
		// Entry source
		private Activity activity;
		private List<Entry> entryList;


		/// <summary>
		/// Initializes a new instance of the <see cref="Labb2.EventAdapter"/> class.
		/// </summary>
		/// <param name="activity">Activity.</param>
		/// <param name="entryList">Entry list.</param>
		public EventAdapter (Activity activity, List<Entry> entryList)
		{
			this.activity = activity;
			this.entryList = entryList;
		}

		/// <Docs>Position of the item whose data we want within the adapter's 
		///  data set.</Docs>
		/// <returns>To be added.</returns>
		/// <para tool="javadoc-to-mdoc">Get the data item associated with the specified position in the data set.</para>
		/// <format type="text/html">[Android Documentation]</format>
		/// <since version="Added in API level 1"></since>
		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="position">Position.</param>
		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}

		/// <Docs>The position of the item within the adapter's data set whose row id we want.</Docs>
		/// <returns>To be added.</returns>
		/// <para tool="javadoc-to-mdoc">Get the row id associated with the specified position in the list.</para>
		/// <format type="text/html">[Android Documentation]</format>
		/// <since version="Added in API level 1"></since>
		/// <summary>
		/// Gets the item identifier.
		/// </summary>
		/// <param name="position">Position.</param>
		public override long GetItemId (int position)
		{
			return position;
		}

		/// <Docs>The position of the item within the adapter's data set of the item whose view
		///  we want.</Docs>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="position">Position.</param>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{

			View view = convertView ?? activity.LayoutInflater.Inflate (Resource.Layout.EventListItem, parent, false);

			view.FindViewById<TextView> (Resource.Id.date_text).Text = entryList [position].Date;
			view.FindViewById<TextView> (Resource.Id.description_text).Text = entryList [position].Description;
			view.FindViewById<TextView> (Resource.Id.amount_text).Text = entryList [position].SalesAmount + " kr/exl.moms";

			return view;


		}

		/// <summary>
		/// Gets the count.
		/// </summary>
		/// <value>The count.</value>
		public override int Count {
			get {
				return entryList.Count;
			}
		}
		
	}
}

