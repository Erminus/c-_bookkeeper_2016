﻿using System;
using System.Collections.Generic;
using Javax.Microedition.Khronos.Opengles;
using System.Linq;
using SQLite;
using Android.Content.Res;

namespace Labb2
{
	public class BookKeeperManager
	{

		// 1.... Make sqlite connection
		private SQLiteConnection entryDb;
		// 2...We need string path to directory of db files
		private string entryPath;

		/// <summary>
		/// Gets the income accounts.
		/// Getting all available from dataBase
		/// </summary>
		/// <value>The income accounts.</value>
		public List<Account> IncomeAccounts {
			get {
				return entryDb.Table<Account> ().Where (a => a.Number >= 3000 && a.Number < 4000).ToList ();
			} 
		}

		/// <summary>
		/// Gets the spend accounts.
		/// Gets all available from dataBase Accounts
		/// </summary>
		/// <value>The spend accounts.</value>
		public List<Account> SpendAccounts {
			get {
				return entryDb.Table<Account> ().Where (a => a.Number >= 5000 && a.Number < 6000).ToList ();
			} 
		}

		/// <summary>
		/// Gets the money accounts.
		/// Gets all available from database account
		/// </summary>
		/// <value>The money accounts.</value>
		public List<Account> MoneyAccounts { 
			get {
				return entryDb.Table<Account> ().Where (a => a.Number >= 1000 && a.Number < 2000).ToList ();
			} 
		}

		/// <summary>
		/// Gets the tax rates.
		/// Gets all available from database TaxRates in the system.
		/// </summary>
		/// <value>The tax rates.</value>
		public List<TaxRate> TaxRates {
			get {
				return entryDb.Table<TaxRate> ().ToList ();
			} 
		}

		/// <summary>
		/// Gets the entries.
		/// Gets all Entries stored from database
		/// </summary>
		/// <value>The entries.</value>
		public List<Entry> Entries {
			get {
				return entryDb.Table<Entry> ().ToList ();
			} 
		}


		/// <summary>
		/// Adds the entry to database
		/// </summary>
		/// <param name="e">E.</param>
		public void AddEntry (Entry e)
		{
			entryDb.Insert (e);
		}


		// Making privat costructor

		private BookKeeperManager ()
		{
			entryPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			entryDb = new SQLiteConnection (entryPath + @"\entryDataBace.db");

			try {
				
				entryDb.Table<Entry> ().Count ();
				entryDb.Table<TaxRate> ().Count ();
				entryDb.Table<Account> ().Count ();

			} catch (Exception ex) {


				entryDb.CreateTable<Entry> ();
				entryDb.CreateTable<TaxRate> ();
				entryDb.CreateTable<Account> ();

				// Default taxRates
				entryDb.Insert (new TaxRate{ Name = "6%", TaxPersentage = 6 });
				entryDb.Insert (new TaxRate{ Name = "12%", TaxPersentage = 12 });
				entryDb.Insert (new TaxRate{ Name = "25%", TaxPersentage = 25 });

				// Default incomeAccounts
				entryDb.Insert (new Account{ Name = "Försäljning", Number = 3000 });
				entryDb.Insert (new Account{ Name = "Försäljning av tjänster", Number = 3040 });
				// Deafult spendAccounts
				entryDb.Insert (new Account{ Name = "Förbrukningsinventarier", Number = 5400 });
				entryDb.Insert (new Account{ Name = "Förbrukningsmaterial", Number = 5300 });
				entryDb.Insert (new Account{ Name = "Övriga egna uttag", Number = 5013 });
				entryDb.Insert (new Account{ Name = "Reklam och PR", Number = 5900 });
				// Default money acounts
				entryDb.Insert (new Account{ Name = "Kassa", Number = 1910 });
				entryDb.Insert (new Account{ Name = "Företagskonto", Number = 1930 });
				entryDb.Insert (new Account{ Name = "Egna insättningar", Number = 1018 });

				Console.WriteLine (ex.Message + " Database created");

			}
				
		}
			
		// Bookkeeper Singelton
		private static BookKeeperManager instance;

		/// <summary>
		/// Gets the Bookkeeper instance
		/// </summary>
		/// <value>The instance.</value>
		public static BookKeeperManager Instance {
			get {
				if (instance == null) {
					instance = new BookKeeperManager ();
				}
				return instance;
			}
		}


	}
}

