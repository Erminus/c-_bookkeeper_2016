﻿using System;
using System.Xml;
using Java.Lang;
using SQLite;

namespace Labb2
{
	public class Account
	{

		[PrimaryKey]
		public int Number{ get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name { get; set; }


		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="Labb2.Account"/>.
		/// Return to string
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="Labb2.Account"/>.</returns>
		public override string ToString ()
		{
			return Name + " (" + Number + ") "; 
		}
	}
}

