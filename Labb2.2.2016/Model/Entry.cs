﻿using System;
using SQLite;
using Android.Text;

namespace Labb2
{
	/// <summary>
	/// Entry class
	/// Handle all user inputs
	/// </summary>
	public class Entry
	{

		/// <summary>
		/// Gets the identifier.
		/// ID is primaryKey for Entry class
		/// </summary>
		/// <value>The identifier.</value>
		[PrimaryKey,AutoIncrement]
		public int Id{ get; private set; }


		/// <summary>
		/// Gets or sets the date.
		///	The date this transaction took place.
		/// Should be on the form "YYYY-MM-DD".
		/// </summary>
		/// <value>The date.</value>
		public string Date { get; set; }

		/// <summary>
		/// Gets or sets the account.
		///	The number of the Account associated with this transaction.
		/// This should always be a MoneyAccount (i.e. 1910, 1930, ...).
		/// </summary>
		/// <value>The account.</value>
		public int Account { get; set; }

		/// <summary>
		/// Gets or sets the type.
		/// The number of the Account associated with this transaction's type.
		/// If this is an income transaction: denotes what kind of sale took place (services, products, ...)
		/// If this is an expense transaction: denotes what kind of purchase took place (withdrawals, advertising, etc products, ...)
		/// </summary>
		/// <value>The type.</value>
		public int Type { get; set; }

		/// <summary>
		/// Gets or sets the description.
		///	A string describing this transaction in detail.
		/// </summary>
		/// <value>The description.</value>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the total amount.
		/// The total amount of this transaction (including sales tax)
		/// </summary>
		/// <value>The total amount.</value>
		public double TotalAmount { get; set; }

		/// <summary>
		/// Gets or sets the tax rate.
		/// The tax rate applying to this transaction (i.e. 6%, 12%, 25%, ...)
		/// </summary>
		/// <value>The tax rate.</value>
		public int TaxRate { get; set; }

		/// <summary>
		/// Gets the tax amount.
		//	Gets the total amount to pay in tax for this transaction.
		/// Is calculated from TaxRate and TotalAmount.
		/// </summary>
		/// <value>The tax amount.</value>
		public double TaxAmount { get { return TotalAmount * TaxRate / 100; } }

		/// <summary>
		/// Gets the sales amount.
		/// Gets the amount of this transaction (excluding sales tax).
		/// Is calculated from TotalAmount and TaxAmount.
		/// </summary>
		/// <value>The sales amount.</value>
		public double SalesAmount { get { return TotalAmount - TaxAmount; } }





	}
}

