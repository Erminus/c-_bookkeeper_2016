﻿using System;
using SQLite;

namespace Labb2
{
	/// <summary>
	/// Tax rate.
	/// </summary>
	public class TaxRate
	{
		/// <summary>
		/// Gets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		[PrimaryKey,AutoIncrement]
		public int Id{ get; private set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name{ get; set; }

		/// <summary>
		/// Gets or sets the tax persentage.
		/// </summary>
		/// <value>The tax persentage.</value>
		public int TaxPersentage{ get; set; }

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="Labb2.TaxRate"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="Labb2.TaxRate"/>.</returns>
		public override string ToString ()
		{
			return "Moms Id: " + Id + " = " + Name;
		}
		
	}
}

