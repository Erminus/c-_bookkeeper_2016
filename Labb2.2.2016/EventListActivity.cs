﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Labb2
{
	[Activity (Label = "EventListActivity")]			
	public class EventListActivity : Activity
	{
		private ListView eventListView;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.EventList);

			// Connecting Adapter to listview
			eventListView = FindViewById<ListView> (Resource.Id.event_listView);
	
			// Telling to eventListView adapter to be myAdapter
			eventListView.Adapter = new EventAdapter (this, BookKeeperManager.Instance.Entries);



		}
	}
}

